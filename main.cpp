/*
 * Travis Joe
 * CS 2
 * Assignment 2.7 - Overtime
 * Calculate an employee's income incl. overtime after fees, tax, etc.
 */
#include <iostream>

using namespace std;

int main() {
    const double HOURLY_WAGE = 16.78; // Hourly wage of an employee
    const double OVERTIME_WAGE = 1.5*HOURLY_WAGE; // Overtime wage of an employee
    const double SS_TAX = 0.06; // Social security tax percentage as a decimal
    const double FEDERAL_INCOME_TAX = 0.14; // Federal income tax percentage as a decimal
    const double STATE_INCOME_TAX = 0.05; // State income tax percentage as decimal
    const int UNION_FEES = 10; // Cost of union fees
    const int HEALTH_INSURANCE_FEE = 35;

    double hoursWorked = 0;
    int dependents = 0;

    double totalPay = 0; // Total pay before taxes and fees

    char shouldContinue = 'y'; // Should the program repeat after the first run?
    
    // Format output to 2 decimal places
    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(2);

    while (shouldContinue == 'y') {
        cout << "Enter the number of hours worked: ";
        cin >> hoursWorked;
        cout << "Enter the number of dependents: ";
        cin >> dependents;

        if (hoursWorked > 40) { // 40 = hours/work week, > is over time
            double overtimeHours = hoursWorked - 40; // Number of overtime hours worked
            double overtimePay = overtimeHours * OVERTIME_WAGE;
            cout << "Overtime hours: " << overtimeHours << "\n";
            cout << "Overtime pay: " << overtimePay << "\n";
            totalPay += overtimePay; // Add overtime pay to total pay

            // Add normal pay for remaining hours
            totalPay += 40 * HOURLY_WAGE;
        } else {
            totalPay += hoursWorked * HOURLY_WAGE;
        }

        cout << "Gross pay before deductions: " << totalPay << "\n";

        // Print amounts of deductions
        double ssTaxDeduction = totalPay * SS_TAX;
        cout << "SS tax: " << ssTaxDeduction << "\n";
        double fedIncTaxDeduction = totalPay * FEDERAL_INCOME_TAX;
        cout << "Federal income tax: " << fedIncTaxDeduction << "\n";
        double stateIncTaxDeduction = totalPay * STATE_INCOME_TAX;
        cout << "State income tax: " << stateIncTaxDeduction << "\n";
        cout << "Union fees: " << UNION_FEES << "\n";

        if (dependents >= 3) {
            totalPay -= HEALTH_INSURANCE_FEE; // Deduct 35 for health insurance fee.
            cout << "Health insurance deduction: " << HEALTH_INSURANCE_FEE << "\n";
        }
        double finalPay = totalPay - ssTaxDeduction - fedIncTaxDeduction - stateIncTaxDeduction - UNION_FEES;

        cout << "Final pay: " << finalPay << "\n\n";

        cout << "Enter \'y\' to run again, or any other key to exit: ";
        cin >> shouldContinue;
    }
    return 0;
}